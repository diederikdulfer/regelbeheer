**PoC Regelbeheer aanpak Belastingdienst (invulling van #37)**

***Regelbeheeraanpak***

De regelbeheer aanpak van de Belastingdienst bestaat uit een aantal stappen:

1. Wetsanalyse
2. Specificeren van regels in RegelSpraak
3. Regelspraakregels valideren in ALEF
4. Vertalen naar service in Blaze

In deze PoC wordt de eerste stap niet uitgevoerd maar wordt gewerkt op basis van de DMN specificaties zoals opgenomen in issue #29.

***ALEF en RegelSpraak***

De Belastingdienst heeft in het kader van wendbare wetsuitvoering een tool ontwikkeld voor het opstellen en testen van regels. 
Deze tool heet ALEF (Agile Law Execution Factory) en is ontwikkeld op basis van Jetbrains MPS. 
In ALEF worden in samenwerking met de domeinexperts specificaties opgesteld. Deze specificaties bestaan uit regels, gegevensdefinities en testgevallen. 
Dit wordt zoveel mogelijk gedaan door de domeinexperts zelf. De regels worden opgesteld in de RegelSpraak taal. 
Dit is een gecontroleerde Nederlandse taal die goed leesbaar is voor juristen als ook voor computersystemen. 
Naast de regels worden ook testgevallen gedefinieerd op basis van de voorbeelden die zijn aangedragen door de domeinexperts. 
Met behulp van de testgevallen worden de regels gevalideerd. ALEF kan de regelanalisten ondersteunen in het bepalen van de testdekking. 
Hiermee kan eenvoudig de mate van vertrouwen in de correctheid van de specificaties worden vastgesteld.

De ALEF tooling maakt het mogelijk om:
- RegelSpraak regels te schrijven
- testgevallen te schrijven
- testgevallen te simuleren
- services te definiëren
- services te genereren

***ALEF project***

In deze map bevindt zich een ALEF project waarin de regels zijn opgenomen voor het bepalen van de IIT. 
Deze regels vormen de basis voor een service die op basis van deze regels worden gegenereerd. De service definitie ziet er als volgt uit:

Invoer:

- Woonplaats
- AOW leeftijd behaald
- Ouder dan 21
- Alleenstaand
- Thuiswonende kinderen
- Inkomen per maand
- Vermogen

Uitvoer:

- Recht beschrijving
- Uit te keren toeslag bedrag

***ALEF IDE***

Om een ALEF project te kunnen openen moet je beschikken over de ALEF IDE. De ALEF IDE is beschikbaar op:

- [Windows](https://drive.google.com/file/d/1J8NgCjbRlbtPI1GwwVwQezS3MF94EAkP/view?usp=sharing)
- [Linux](https://drive.google.com/file/d/1xqNp_nyblP7JM7QnwD9Hu0Va1UprjSou/view?usp=sharing)
- [Mac](https://drive.google.com/file/d/1rL_4fpAKuJJjIRuIA6Ufhk7vvmSBVsrl/view?usp=sharing )

Bovenstaande links verwijzen naar een archief. Download het archief en pak het archief uit op een lokale schijf. 

Voor windows:
Start ALEF vanuit de bin directory d.m.v. het alef batch bestand.

Voor mac:
Na het uitpakken is een alef app beschikbaar die kan worden opgestart.
