<?xml version="1.0" encoding="UTF-8"?>
<solution name="mdrs_vil.build" uuid="3fbb87cd-c667-40a4-a005-c4b2011978f6" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">422c2909-59d6-41a9-b318-40e6256b250f(jetbrains.mps.ide.build)</dependency>
    <dependency reexport="false">3ae9cfda-f938-4524-b4ca-fbcba3b0525b(com.mbeddr.platform)</dependency>
    <dependency reexport="false">1fc41867-980a-4b05-8e58-ecab42f97613(build_alef)</dependency>
    <dependency reexport="false">91ae349b-fed8-4544-9825-5114cd70b286(linguistics.platform)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:798100da-4f0a-421a-b991-71f8c50ce5d2:jetbrains.mps.build" version="0" />
    <language slang="l:0cf935df-4699-4e9c-a132-fa109541cba3:jetbrains.mps.build.mps" version="7" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
  </languageVersions>
  <dependencyVersions>
    <module reference="1fc41867-980a-4b05-8e58-ecab42f97613(build_alef)" version="0" />
    <module reference="3ae9cfda-f938-4524-b4ca-fbcba3b0525b(com.mbeddr.platform)" version="0" />
    <module reference="422c2909-59d6-41a9-b318-40e6256b250f(jetbrains.mps.ide.build)" version="0" />
    <module reference="91ae349b-fed8-4544-9825-5114cd70b286(linguistics.platform)" version="0" />
    <module reference="3fbb87cd-c667-40a4-a005-c4b2011978f6(mdrs_vil.build)" version="0" />
  </dependencyVersions>
</solution>

