<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9323a62c-c9bd-4ca3-a074-f1b4670ff741(Parameters)">
  <persistence version="9" />
  <languages>
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports>
    <import index="6mfl" ref="r:7240dd22-cd7f-41e2-842a-563e8dc036ef(Gegevens)" />
  </imports>
  <registry>
    <language id="471364db-8078-4933-b2ef-88232bfa34fc" name="gegevensspraak">
      <concept id="5478077304742291705" name="gegevensspraak.structure.DatumTijdLiteral" flags="ng" index="2ljiaL">
        <property id="5478077304742291708" name="jaar" index="2ljiaO" />
      </concept>
      <concept id="5478077304742085581" name="gegevensspraak.structure.Geldigheidsperiode" flags="ng" index="2ljwA5">
        <child id="5478077304742085582" name="van" index="2ljwA6" />
      </concept>
      <concept id="7037334947758876146" name="gegevensspraak.structure.Parameterset" flags="ng" index="vdosF">
        <child id="7037334947758876149" name="toekenning" index="vdosG" />
        <child id="3122098214253204654" name="geldig" index="3H8BXA" />
      </concept>
      <concept id="558527188464633210" name="gegevensspraak.structure.AbstractNumeriekeLiteral" flags="ng" index="3e5kNY">
        <property id="558527188465081158" name="waarde" index="3e6Tb2" />
      </concept>
      <concept id="5917060184176395023" name="gegevensspraak.structure.Parametertoekenning" flags="ng" index="1Er9RG">
        <reference id="5917060184176396258" name="param" index="1Er9$1" />
        <child id="2445565176094168041" name="waarde" index="HQftV" />
      </concept>
      <concept id="5917060184181965945" name="gegevensspraak.structure.NumeriekeLiteral" flags="ng" index="1EQTEq" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="vdosF" id="6PDUWJHlm9k">
    <property role="TrG5h" value="parameterset 2021" />
    <node concept="2ljwA5" id="6PDUWJHlm9l" role="3H8BXA">
      <node concept="2ljiaL" id="6PDUWJHlm9m" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
      </node>
    </node>
    <node concept="1Er9RG" id="6PDUWJHlm9$" role="vdosG">
      <ref role="1Er9$1" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
      <node concept="1EQTEq" id="6PDUWJHlm9D" role="HQftV">
        <property role="3e6Tb2" value="1207,30" />
      </node>
    </node>
    <node concept="1Er9RG" id="6PDUWJHlmaw" role="vdosG">
      <ref role="1Er9$1" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
      <node concept="1EQTEq" id="6PDUWJHlmaB" role="HQftV">
        <property role="3e6Tb2" value="1724,71" />
      </node>
    </node>
    <node concept="1Er9RG" id="6PDUWJHlmb6" role="vdosG">
      <ref role="1Er9$1" to="6mfl:6PDUWJHlm6L" resolve="BOVENGRENS_VERMOGEN_LAAG" />
      <node concept="1EQTEq" id="6PDUWJHlmbf" role="HQftV">
        <property role="3e6Tb2" value="6225,00" />
      </node>
    </node>
    <node concept="1Er9RG" id="6PDUWJHlmbQ" role="vdosG">
      <ref role="1Er9$1" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
      <node concept="1EQTEq" id="6PDUWJHlmc1" role="HQftV">
        <property role="3e6Tb2" value="12450,00" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYWj" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYIG" resolve="120% VAN DE BIJSTANDSNORM 18-20 ALLEENSTAANDE (OUDER) " />
      <node concept="1EQTEq" id="3vw$xkYGYWG" role="HQftV">
        <property role="3e6Tb2" value="875,71" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYXB" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYNc" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD BEIDE JONGER DAN 21 MET KIND" />
      <node concept="1EQTEq" id="3vw$xkYGYY4" role="HQftV">
        <property role="3e6Tb2" value="955,61" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYZ3" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYQJ" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD EEN JONGER DAN 21 MET KIND" />
      <node concept="1EQTEq" id="3vw$xkYGYZ$" role="HQftV">
        <property role="3e6Tb2" value="1528,66" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYU$" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGXfm" resolve="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
      <node concept="1EQTEq" id="3vw$xkYGYUR" role="HQftV">
        <property role="3e6Tb2" value="1226,00" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYU3" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYy0" resolve="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD GEZIN of SAMENWONENDE" />
      <node concept="1EQTEq" id="3vw$xkYGYUk" role="HQftV">
        <property role="3e6Tb2" value="1751,42" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYVG" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYE0" resolve="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
      <node concept="1EQTEq" id="3vw$xkYGYW3" role="HQftV">
        <property role="3e6Tb2" value="1363,40" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYV7" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYDY" resolve="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD GEZIN of SAMENWONENDE" />
      <node concept="1EQTEq" id="3vw$xkYGYVs" role="HQftV">
        <property role="3e6Tb2" value="1847,64" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYWW" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYII" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD BEIDE JONGER DAN 21" />
      <node concept="1EQTEq" id="3vw$xkYGYXn" role="HQftV">
        <property role="3e6Tb2" value="605,32" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYYk" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGYOf" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD EEN JONGER DAN 21" />
      <node concept="1EQTEq" id="3vw$xkYGYYN" role="HQftV">
        <property role="3e6Tb2" value="1178,38" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYS7" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
      <node concept="1EQTEq" id="3vw$xkYGYSk" role="HQftV">
        <property role="3e6Tb2" value="12590,00" />
      </node>
    </node>
    <node concept="1Er9RG" id="3vw$xkYGYS$" role="vdosG">
      <ref role="1Er9$1" to="6mfl:3vw$xkYGX_3" resolve="VERMOGENSGRENS ALLEENSTAANDE" />
      <node concept="1EQTEq" id="3vw$xkYGYSN" role="HQftV">
        <property role="3e6Tb2" value="6295,00" />
      </node>
    </node>
  </node>
</model>

