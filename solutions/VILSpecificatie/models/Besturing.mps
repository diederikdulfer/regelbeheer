<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e66d6402-1c81-4d02-b573-4fd1d2d735a4(Besturing)">
  <persistence version="9" />
  <languages>
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports>
    <import index="dju2" ref="r:9f7764e1-81c7-4150-a011-8bdc0e8469c7(Regels)" />
    <import index="6mfl" ref="r:7240dd22-cd7f-41e2-842a-563e8dc036ef(Gegevens)" />
  </imports>
  <registry>
    <language id="471364db-8078-4933-b2ef-88232bfa34fc" name="gegevensspraak">
      <concept id="5478077304742085581" name="gegevensspraak.structure.Geldigheidsperiode" flags="ng" index="2ljwA5" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="65239ca4-9057-41f8-999d-97fa1a60b298" name="besturingspraak">
      <concept id="9154144551726427366" name="besturingspraak.structure.FlowVersie" flags="ng" index="1Fci4u">
        <property id="8967493964168670222" name="declaratief" index="3vMlKL" />
        <child id="2800963173599034005" name="geldig" index="2DzjYZ" />
        <child id="9154144551726427489" name="body" index="1Fci2p" />
      </concept>
      <concept id="8556987547900021295" name="besturingspraak.structure.Flow" flags="ng" index="3MLgNT">
        <reference id="8556987547900099829" name="onderwerp" index="3MLzCz" />
        <child id="9154144551726427484" name="versie" index="1Fci2$" />
      </concept>
      <concept id="8556987547900055494" name="besturingspraak.structure.RuleTask" flags="ng" index="3MLC$g">
        <reference id="8556987547900055495" name="rule" index="3MLC$h" />
      </concept>
      <concept id="8556987547900057353" name="besturingspraak.structure.Sequence" flags="ng" index="3MLD7v">
        <child id="8556987547900057354" name="stap" index="3MLD7s" />
      </concept>
    </language>
  </registry>
  <node concept="3MLgNT" id="6PDUWJHkRWZ">
    <property role="TrG5h" value="Bepalen individuele inkomenstoeslag" />
    <ref role="3MLzCz" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
    <node concept="1Fci4u" id="6PDUWJHkRX0" role="1Fci2$">
      <property role="3vMlKL" value="true" />
      <node concept="3MLD7v" id="6PDUWJHkRX1" role="1Fci2p">
        <node concept="3MLC$g" id="6PDUWJHkRX3" role="3MLD7s">
          <ref role="3MLC$h" to="dju2:6PDUWJHkKMM" resolve="Toekennen kenmerken Utrecht" />
        </node>
        <node concept="3MLC$g" id="6PDUWJHkRX8" role="3MLD7s">
          <ref role="3MLC$h" to="dju2:6PDUWJHkHID" resolve="Uit te keren individuele inkomenstoeslag" />
        </node>
        <node concept="3MLC$g" id="6PDUWJHkWc5" role="3MLD7s">
          <ref role="3MLC$h" to="dju2:6PDUWJHkRXc" resolve="Recht beschrijving" />
        </node>
      </node>
      <node concept="2ljwA5" id="6PDUWJHkRX2" role="2DzjYZ" />
    </node>
  </node>
  <node concept="3MLgNT" id="1qYcgYdZkgo">
    <property role="TrG5h" value="Bepalen bijdrage voor sport en cultuur" />
    <ref role="3MLzCz" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
    <node concept="1Fci4u" id="1qYcgYdZkgp" role="1Fci2$">
      <property role="3vMlKL" value="true" />
      <node concept="3MLD7v" id="1qYcgYdZkgq" role="1Fci2p">
        <node concept="3MLC$g" id="1qYcgYdZkgs" role="3MLD7s">
          <ref role="3MLC$h" to="dju2:1qYcgYdYAKo" resolve="Toekennen kenmerken Amersfoort" />
        </node>
        <node concept="3MLC$g" id="1qYcgYdZkgx" role="3MLD7s">
          <ref role="3MLC$h" to="dju2:3vw$xkYH054" resolve="Uit te keren bijdrage voor sport en cultuur" />
        </node>
      </node>
      <node concept="2ljwA5" id="1qYcgYdZkgr" role="2DzjYZ" />
    </node>
  </node>
</model>

